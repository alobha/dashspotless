<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMainServicesBanners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_service_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('main_service_id')->index();
            $table->string('avtar_name')->index();
            $table->string('avtar_path')->index();
            $table->boolean('status')->index()->default(false);
            $table->unsignedInteger('added_by')->index();
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_service_banners');
    }
}

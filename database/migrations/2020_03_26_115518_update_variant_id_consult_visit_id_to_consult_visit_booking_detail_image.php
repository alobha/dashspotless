<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVariantIdConsultVisitIdToConsultVisitBookingDetailImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('consult_visit_booking_details_images', 'variant_id')){
            Schema::table('consult_visit_booking_details_images', function (Blueprint $table) {
                 $table->renameColumn('variant_id', 'consult_visit_booking_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consult_visit_booking_details_images', function (Blueprint $table) {
            $table->renameColumn('consult_visit_booking_id', 'variant_id');
        });
    }
}

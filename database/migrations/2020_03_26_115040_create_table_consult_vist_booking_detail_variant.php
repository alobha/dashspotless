<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConsultVistBookingDetailVariant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_visit_booking_detail_variant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedinteger('consult_visit_id')->index();
            $table->unsignedinteger('variant_id')->index();
            $table->unsignedInteger('added_by')->index()->default(0);
            $table->unsignedInteger('updated_by')->index()->default(0);
            $table->unsignedInteger('deleted_by')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consult_visit_booking_detail_variant');
    }
}

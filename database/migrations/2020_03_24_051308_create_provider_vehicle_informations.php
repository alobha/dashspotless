<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderVehicleInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_vehicle_informations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->index();
            $table->unsignedInteger('vehicle_type')->default(0)->index();
            $table->string('vehicle_owner_first_name')->index()->nullable();
            $table->string('vehicle_owner_middle_name')->index()->nullable();
            $table->string('vehicle_owner_last_name')->index()->nullable();
            $table->string('vehicle_chasis_number')->index()->nullable();
            $table->string('vehicle_number')->index()->nullable();
            $table->string('vehicle_image_name')->index()->nullable();
            $table->string('vehicle_image_path')->index()->nullable();
            $table->string('rc_front_image')->index()->nullable();
            $table->string('rc_back_image')->index()->nullable();
            $table->string('insur_certi_front_image')->index()->nullable();
            $table->string('insur_certi_back_image')->index()->nullable();
            $table->string('poll_certifi_front_image')->index()->nullable();
            $table->string('poll_certifi_back_image')->index()->nullable();
            $table->unsignedInteger('vehicle_status')->default(0);
            $table->unsignedInteger('verified_by')->default(0);
            $table->unsignedInteger('added_by')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_vehicle_informations');
    }
}

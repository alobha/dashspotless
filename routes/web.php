<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//      return redirect(route('login'));
// });

// Auth::routes();
Route::get('admin-login', 'AdminController\LoginController@index');
Route::post('admin-login', 'AdminController\LoginController@login');

Route::group(['middleware' => 'admin'], function(){

Route::get('/home', 'HomeController@index')->name('home');

# category routes
Route::get('category', 'AdminController\CategoryController@index');
Route::post('add-category', 'AdminController\CategoryController@store');
Route::get('edit-category/{id}', 'AdminController\CategoryController@update');
Route::post('edit-category/{id}', 'AdminController\CategoryController@edit');
Route::delete('delete-category/{id}', 'AdminController\CategoryController@delete');
Route::post('status-category/{id}', 'AdminController\CategoryController@status');

# sub category routes
Route::get('sub-category', 'AdminController\SubCategoryController@index');
Route::post('add-sub-category', 'AdminController\SubCategoryController@store');
Route::get('edit-sub-category/{id}', 'AdminController\SubCategoryController@update');
Route::post('edit-sub-category/{id}', 'AdminController\SubCategoryController@edit');
Route::delete('delete-sub-category/{id}', 'AdminController\SubCategoryController@delete');
Route::post('status-sub-category/{id}', 'AdminController\SubCategoryController@status');





# Variants
Route::get('variant-create', 'AdminController\SubCategoryController@create');







// logout session
Route::get('logout','AdminController\LoginController@logout');

});
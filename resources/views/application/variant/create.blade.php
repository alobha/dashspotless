  @extends('application.layouts.app')
@section('title','Category')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Variant
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Variant</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="heading-bg" style="margin-top: 30px;">
            <h3><i class="fa fa-circle-o"></i>Add Variant</h3>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control" id="" name="">
                      <option>Select Categories</option>
                      <option>Car Wash</option>
                      <option>Office Cleaning</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Category Name<span class="required">*</span></label>

                  <div class="col-sm-10">
                    <select class="form-control" id="" name="">
                      <option>Select Sub Categories</option>
                      <option>Car Wash</option>
                      <option>Office Cleaning</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Subject Line</label>

                  <div class="col-sm-10">
                   <input type="text" name="" id="" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Field Type</label>

                  <div class="col-sm-10">
                   <label class="radio-inline"><input type="radio" name="optradio" id="noCheck" checked onclick="javascript:yesnoCheck();">Radio Button</label>
                  <label class="radio-inline"><input type="radio" name="optradio" id="yesCheck" onclick="javascript:yesnoCheck();">Checkbox</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Variant Name</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-6">
                         <input type="text" name="" id="" class="form-control">
                      </div>
                      <div class="col-sm-4">
                        <label class="checkbox-inline"><input type="checkbox" name="">Consultant Visit</label>
                      </div>
                      <div class="col-sm-2">
                        <button type="button" class="btn btn-success m-t-32 mr-2" id="add_variant">Add More</button>
                      </div>
                    </div>
                    <div id="add_variantbox" class="add_variantbox"></div>
                  </div>
                </div>

                <div class="consultant_booking mt-30" id="consultant_booking">
                <h3>Consultant Visit Booking Details & Conditions</h3>
                <div class="form-group">
            <label for="" class="col-sm-2 control-label">Subject Line</label>
            <div class="col-sm-10">
            <input type="text" name="" id=""  class="form-control">
          </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Consultant Amount</label>
            <div class="col-sm-10">
            <input type="text" name="" id=""  class="form-control">
          </div>
          </div>
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Reference Image</label>
          <div class="col-sm-10" id="banner_row">
            <div class="row addrow">
              <div class="col-sm-10"><input type="file" name="" id="banner" class="form-control"></div>
              <div class="col-sm-2 text-right">
         <button type="button" class="btn btn-success m-t-32 mr-2" id="add_details_banner"><i class="fa fa-plus"></i></button>

        </div>
            </div>
          <div id="service_part" class="details_service">
              
              </div>
          </div>
         </div>
         <div class="form-group">
            <label for="" class="col-sm-2 control-label">Discriptions</label>
            <div class="col-sm-10">
            <textarea class="form-control"></textarea>
          </div>
          </div>
          <h4>Terms And Conditions</h4>
          <div class="form-group">
           <label for="" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input type="text" name="" id="banner" class="form-control">
         </div>
       </div>
       <div class="form-group">
           <label for="" class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea class="form-control" id="" name=""></textarea>
         </div>

       <div class="col-sm-12 text-right mt-10">
      <button type="button" class="btn btn-success m-t-32 mr-2" id="add_bullet">Add More</button>
          
           </div>
       </div>
       <div id="bullet_part" class="bullet_part">
     
              
              </div>
       
              </div>

                <div class="form-group mt-30">

                  <div class="col-sm-12 text-center">
                   <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Submit</button>
                  </div>
                </div>
              </div>
            </form>
               
                </div>
              </div>
            </form>
        </div>
        <div class="col-md-1"></div>
      </div>
            </div>
            <!-- /.box-body -->


  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('body-scripts')
   
       <!--------Inrtoduction------>
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_charges').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="" id="banner123" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#charges').append(html);
            });
      
      $('.optionBox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_expert').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="" id="expert_part" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#expert').append(html);
            });
      $('.expert_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  <!-----------Select Variant-->
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_more').on('click', function(){
      
                var html = '<div class="row addrow"><div class="form-group"><label for="" class="col-sm-2 control-label">Enter Statement</label><div class="col-sm-10"><input type="text" name="" id=""  class="form-control"></div></div>'+ 
          '<div class="form-group"><label for="" class="col-sm-2 control-label">Enter Label</label><div class="col-sm-10"><input type="text" name="" id="" class="form-control"></div></div>'+
                '<div class="form-group"><label for="" class="col-sm-2 control-label">Count</label><div class="col-sm-10"><button class="btn btn-default">1</button><button class="btn btn-default">2</button><button class="btn btn-default">3</button><button class="btn btn-default">4</button><button class="btn btn-default">5</button><button class="btn btn-default">6</button><button class="btn btn-default">7</button><button class="btn btn-default">8</button><button class="btn btn-default">9</button><button class="btn btn-default">10</button></div>  </div>'+
                '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#variant').append(html);
            });
      $('.variant_new').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    
    
    </script>
  <!----------Add Service Details-------->
    <script type="text/javascript">
        $(document).ready(function(){
            $('#add_details_banner').on('click', function(){
      
                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="" id="banner123" class="form-control"></div>'+
                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#service_part').append(html);
            });
      $('.details_service').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>

  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_bullet').on('click', function(){
      
                var html = '<div class="bullet_part"><div class="form-group"><label for="" class="col-sm-2 control-label">Title</label><div class="col-sm-10"><input type="text" name="" id="banner" class="form-control"></div></div><div class="form-group"><label for="" class="col-sm-2 control-label">Description</label><div class="col-sm-10"><textarea class="form-control" id="" name=""></textarea></div></div><div class="form-group text-right"><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
                $('#bullet_part').append(html);
            });
      $('.bullet_part').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>
  
  <script type="text/javascript">
        $(document).ready(function(){
            $('#add_variant').on('click', function(){
      
                var html = '<div class="row mt-10"><div class="col-sm-6"><input type="text" name="" id="" class="form-control"></div><div class="col-sm-4"><label class="checkbox-inline"><input type="checkbox" name="">Consultant Visit</label></div><div class="col-sm-2"><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button></div></div>';
                $('#add_variantbox').append(html);
            });
      $('.add_variantbox').on('click','.remove',function() {
          $(this).parent().remove();
          });
        });
    </script>

    <script type="text/javascript">
     function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('consultant_booking').style.display = 'block';
    }
    else document.getElementById('consultant_booking').style.display = 'none';

}
    </script>




 
@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    # Bind view page
 protected $view = 'application.dashboard.';
 
 /**
  * show dashboard page
  * @param
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
    # return the dashboard page
    return view($this->view.'index');
 } 
}

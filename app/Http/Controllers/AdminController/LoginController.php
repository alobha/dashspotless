<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
    	return view('application.layouts.login');
    }

    public function login(Request $request)
    {
       //dd($request->all());
    	$v = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        if ($v->fails()) {  
            return 'Please Fill All Entries';
        }

        $credentials = $request->only('email', 'password');
        //dd($credentials);
        if (!Auth::guard('admin')->attempt($credentials)) {
            return redirect()->back()->with('invalid_msg','Given Credentials Are Not Matches');
        } else {
          
            $user = Auth::guard('admin')->user();
            //dd('success');
            if($user->deleted_at == Null)
                 {
              	 return redirect('home');
                 }
                 else{
                    return redirect()->back();
                 }
              }
          return 1;
    }
    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('admin-login');
    }
}

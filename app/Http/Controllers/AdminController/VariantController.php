<?php

namespace App\Http\Controllers\AdminController;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\MessageStatusTrait;

class VariantController extends Controller
{
 use MessageStatusTrait;

 # Bind Type
 protected $type = 'Variant';

 # Bind location
 protected $view = 'application.variant.';

 # Bind variant
 protected $variant;

 /**
  * default constructor
  * @param
  * @return
  */
 function __construct(Variant $variant)
 {
 	$this->variant = $variant;
 }
 


 /**
  * create page of variant
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function create()
 {
  # fetch variant list
  $category = Category::orderBy('name')
                     ->get();

  # return to create page
  return view($this->view.'create')->with(['category' => $category]);;
 }








 /**
  * index page of variant
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function index()
 {
 	# fetch variant list
 	$variants = $this->variant
 	                   ->orderBy('name')
 	                   ->get();

 	# return to index page
 	return view($this->view.'index')->with(['variants' => $variants]);;
 }

 /**
  * create variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function store(Request $request)
 {
 	# current user
 	$user = Auth::guard('admin')->user();

  $query = $this->variant;

 	# upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $filename =$date.'_'.$randNumber.'_'.$name;
     $file->move('dist/img/variant/', $filename);
     $avtarPath ='dist/img/variant/'.$filename;
    } else {
     $avtarPath  = null;  
    }

 	# request param
 	$arrayData = [
                  'name'       => $request->name ?? null,
                  'avtar_name' => $request->avtar_name ?? null,
                  'avtar_path' => $avtarPath,
                  'added_by'   => $user->id ?? null
 	];

  # check the requested variant already exist or not
  $countvariant = $query->where('name', $request->name)->count();
  
  if ($countvariant == 0) {
    #store 
    $createvariant = $query->create($arrayData);

    # check created or not
    # if created
    if ($createvariant) {
     # return successs
     $output = ['success' => 200, 'message' => 'variant Added Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 } 

 /**
  * edit  page
  * @param Illuminate\Http\Request;
  * @return Illuminate\Http\Response;
  */
 public function update($id)
 {
  # Fetch variant by id
  $variant = $this->variant
                   ->where('id', $id)
                   ->first(); 
 	# code...
 	return view($this->view.'edit')->with(['variant' => $variant]);
 }

 /**
  * edit variant
  * @param Illuminate\Http\Request; 
  * @return Illuminate\Http\Response;
  */
 public function edit(Request $request, $id)
 {
  # current user
  $user = Auth::guard('admin')->user();

  $query = $this->variant;

  # upload avtar
    if($request->hasfile('avtar_name')) {
     $file = $request->file('avtar_name');
     $name = $file->getClientOriginalName(); // getting image name
     $date = date('y-m-d');              
     $randNumber = rand(0000,9999);              
     $filename =$date.'_'.$randNumber.'_'.$name;
     $file->move('dist/img/variant/', $filename);
     $avtarPath ='dist/img/variant/'.$filename;
    } else {
     $avtarPath = $query->where('id', $id)->first()->avtar_path; 
    }

  # request param
  $arrayData = [
                  'name'       => $request->name ?? null,
                  'avtar_name' => $request->avtar_name ?? null,
                  'avtar_path' => $avtarPath,
                  'updated_by' => $user->id ?? null
  ];

  # check the requested variant already exist or not
  $countvariant = $query->where('name', $request->name)
                         ->where('id', '!=', $id)
                         ->count();
  
  if ($countvariant == 0) {
    #store 
    $updatevariant = $query->where('id', $id)->update($arrayData);

    # check created or not
    # if created
    if ($updatevariant) {
     # return successs
     $output = ['success' => 200, 'message' => 'variant update Successfully'];
    } else {
     # return error
     $output = ['error' => 100, 'message' => 'Something went wrong'];
    }
  } else {
    # return exist message
    $output = ['error' => 100, 'message' => 'Already Exist'];
  }
  return $output;
 }  

 /**
  * delete variant
  * @param $id
  * @return \Illuminate\Http\Response
  */
 public function delete($id)
 {
   $query = $this->variant;

   # current user
   $user = Auth::guard('admin')->user();

   #update deleted by
   $query->where('id', $id)->update(['deleted_by' => $user->id]);

   # delete variant by id
   $query->where('id', $id)->delete();
   
   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $this->deleteMessage($this->type)];
 }

 /**
  * active deactive
  * @param $id
  * @return \Illuminate\Http\Response
  */ 
 public function status($id)
 {
   # initiate constructor
   $query =  $this->variant;

   # get the status 
   $status =  $query->where('id', $id)->first()->status;

   # check status, if active
   if ($status == '1') {
     #message
     $message = $this->inActiveMessage($this->type);

     # deactive( update status to zero)
     $statusCode = '0';
   } else {
     #message
     $message = $this->activeMessage($this->type);

     # active( update status to one)
     $statusCode = '1';
   }
   
   # update status code
   $query->where('id', $id)->update(['status' => $statusCode]);

   # return success
   return  [$this->successKey  =>  $this->successStatus,  $this->messageKey  => $message];
 }
}

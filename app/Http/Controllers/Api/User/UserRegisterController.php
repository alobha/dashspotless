<?php

namespace App\Http\Controllers\Api\User;

use DB;
use Validator;
use App\Models\User; 
use App\Models\UserOtp; 
use App\Models\Category; 
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\StatusTrait;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 

class UserRegisterController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $userOtp;

    # bind Category Model
    protected $category;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, UserOtp $userOtp, Category $category)
    {
        $this->user     = $user;
        $this->userOtp  = $userOtp;
        $this->category  = $category;
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'device_type'       => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        DB::beginTransaction();

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check user already Exist on that Email
        $users = $this->user->where('mobile_number', $mobileNumber)->get();

        
        # return response if User already exist on requested Email
        if($users->isNotEmpty()) {
            $otp = mt_rand(1000,9999);

            # fetch the First User
            $user = $users->first();

            # Update User Device Type
            $user->update(['device_type' => $request->get('device_type')]);

            # Set the success message after User creation 
            $data = [
                'user_id'           =>  (string)$user->id,
                'mobile_number'     =>  (string)$user->mobile_number,
                'otp'               =>  (string)$otp
            ];

            $otpData   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($otpData);

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User already exist!!!',
                'data'      =>  $data
             ]); 
        } else {
            # Create User Model
            $user =  $this->user->create([
                'mobile_number' => $mobileNumber,
                'device_type' => $request->get('device_type')
            ]);

            #Set data for User Otp
            $otp    = mt_rand(1000,9999);
            $data   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($data);

            # Set the success message after User creation 
            $data = [
                'mobile_number'     =>  $user->mobile_number,
                'userId'            =>  (string)$user->id,
                'otp'               =>  (string)$otp
            ];

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User has been register Successfully!!!',
                'data'      => $data
             ]); 

        }
    }

    /**
     * @method to verify Users Request Otp
     * 
     * @return Otp Verified or Not
     */
    public function verifyOtp(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'otp'               => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        $mobileNumber = $input['mobile_number'];

        # check user already Exist on that Email
        $users = $this->user
                      ->with('otp')
                      ->where('mobile_number', $mobileNumber)
                      ->get();

        # return response if User already exist on requested Email
        if($users->isNotEmpty()) {
            # fetch User
            $user = $users->first();

            # fetch the Otp of Users
            $userOtps = $user->otp;
            if($userOtps->isNotEmpty() ) {
                $lastOtp = $userOtps->last()->otp;
                if($lastOtp == $request->get('otp')) {
                    $token = $this->generateToken();

                    # Update uSer token
                    $user->update(['api_token' => $token]);

                    # Set the Data
                    $data = [
                        'token'             => $token,
                        'mobile_number'     => $mobileNumber,
                        'user_id'           => $user->id
                    ];

                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Otp has been Verified.',
                        'data'      => $data
                     ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'Otp does not match.',
                        'data'      => []
                     ]);
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Otp Not Found.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with this Mobile Number.',
                'data'      => []
             ]); 
        }

    }
    
    /**
     * function to generate the Token
     * 
     * @return Token
     */
    public function generateToken()
    {
        # Set the token 
        $token = Str::random(60);

        # Hash Token
        $hashToken = hash('sha256', $token);

        # return the Hash Token 
        return $hashToken;
    }

    /**
     * @method to fetch all the Categories
     * 
     * @return Json data
     */
    public function categories(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # feth the user
        $user = $this->user->find($request->get('user_id'));

        # check for user
        if($user != '') {
            $categories = $this->getCategories();
            if($categories->isNotEmpty()) {
                $categoriesData = [];
                foreach ($categories as $key => $category) {
                    $data = [
                        'category_id'   => (string)$category->id,
                        'name'          => (string)$category->name,
                        'avtar_path'    => (string)$category->avtar_path,
                    ];

                    array_push($categoriesData, $data);
                }

                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'Categories Found.',
                    'data'      => $categoriesData
                 ]); 

            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'No category Found.',
                    'data'      => []
                ]); 
            }
        } else {
           # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found with provided Id.',
                'data'      => []
             ]); 
        }
    }

    /**
     * fetch all the categories avail in System
     * 
     * @return Category Collection
     */
    public function getCategories()
    {
        # Fetch all the Active Categories
        $categories = $this->category->active()->get();

        # return 
        return $categories;
    }

    /**
     * @method to Get th eSub Catgeories on basis of Category
     * 
     * @return Json
     */
    public function subCategories(Request $request)
    {
         # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'       => 'required|numeric',
            'category_id'   => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch the Category
        $category = $this->category->find($request->get('category_id'));

        # check Category is found or not
        if($category != '') {
            $subCategories = $this->getSubCategories($category);

            # check for Sub categories
            if($subCategories->isNotEmpty()) {
                foreach ($subCategories as $key => $subCategory) {
                    # get the banners of Sub Category
                    $banners = $subCategories->banners;

                    # get the Expertise In 
                    $expertiseIn = $subCategories->expertiseIn;
                }
            } else {
                # return response
                return response()->json([
                   'code'      => (string)$this->failedStatus, 
                   'message'   => 'No Sub category Found on provided category id',
                   'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
               'code'      => (string)$this->failedStatus, 
               'message'   => 'No category Found on provide category id',
               'data'      => []
            ]); 
        }
    }

    /**
     * fetch all the Sub-categories avail in System
     * 
     * @return Sub Category Collection
     */
    public function getSubCategories($category)
    {
        # Set Relation
        $relations = ['subCategories.banners', 'subCategories.expertiseIn'];

        # Fetch all the Active Categories
        $subCategories = $this->category
                              ->with($relations)
                              ->active()
                              ->get();

        # return 
        return $subCategories;
    }
}

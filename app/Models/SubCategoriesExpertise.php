<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoriesExpertise extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_categories_expertise_in';
  
  # define fillable fildes
  protected $fillable = [
  	                   'sub_category_id',
  	                   'title',
  	                   'avtar_name',
  	                   'avtar_path',
  	                   'status',
  	                   'added_by',
  	                   'updated_by',
  	                   'deleted_by'
  ];
}

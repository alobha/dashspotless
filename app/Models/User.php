<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    //protected $guard = 'api';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name', 
        'last_name',
        'mobile_number',
        'email',
        'status',
        'age',
        'gender',
        'latitude',
        'longitude',
        'device_type',
        'android_key',
        'ios_key',
        'password',
        'api_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Model has Many Otp
     * 
     * @return relation
     */
    public function otp()
    {
        return $this->hasMany('App\Models\UserOtp', 'user_id', 'id');
    }
    
}
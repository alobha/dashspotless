<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='categories';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  	                   'avtar_name',
  	                   'avtar_path',
  	                   'status',
  	                   'added_by',
  	                   'updated_by',
  	                   'deleted_by'
  ];

  /**
   * Modal has Many Sub Categories
   * 
   * @retrun relation
   */
  public function subCategories()
  {
    return $this->hasMany('App\Models\Subcategory', 'category_id', 'id');
  }
  
  /**
    * Scope a query to only include active users.
    *
    * @param  \Illuminate\Database\Eloquent\Builder  $query
    * @return \Illuminate\Database\Eloquent\Builder
  */
  public function scopeActive($query)
  {
      return $query->where('status', 1);
  }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
  use SoftDeletes;
  
  # define table
  protected $table ='sub_categories';
  
  # define fillable fildes
  protected $fillable = [
  	                   'category_id',
  	                   'name',
                       'title',
                       'discount_label',
  	                   'avtar_name',
  	                   'avtar_path',
  	                   'status',
                       'is_variant_avail',
  	                   'added_by',
  	                   'updated_by',
  	                   'deleted_by'
  ];

  /**
   * relation with category
   * @param
   * @return
   */
  public function category()
  {
    # code...
    return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
  }

  /**
   * relation with category
   * @param
   * @return
   */
  public function subCategoryBanner()
  {
    # code...
    return $this->belongsTo(\App\Models\SubCategoryBanner::class, 'id', 'sub_category_id');
  }

  /**
   * relation with category
   * @param
   * @return
   */
  public function subCategoryExpertise()
  {
    # code...
    return $this->belongsTo(\App\Models\SubCategoriesExpertise::class, 'id', 'sub_category_id');
  }
  
}
